package com.example.game

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.widget.Toast
import com.example.game.R.drawable.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        newGame()


        newG.setOnClickListener {
            newGame()

        }

    }

    private fun newGame() {
        chronometer.base = SystemClock.elapsedRealtime()
        chronometer.start()
        val images: MutableList<Int> =
            mutableListOf(
                frog,
                giraffe,
                turtle,
                panda,
                penguin,
                snake,
                frog,
                giraffe,
                turtle,
                panda,
                penguin,
                snake
            )

        val buttons = arrayOf(
            button1,
            button2,
            button3,
            button4,
            button5,
            button6,
            button7,
            button8,
            button9,
            button10,
            button11,
            button12
        )
        val cardBack = question
        var clicked = 0
        var turnOver = false
        var lastClicked = -1

        images.shuffle()

        for (i in buttons.indices) {
            buttons[i].setBackgroundResource(cardBack)
            buttons[i].text = "cardBack"
            buttons[i].textSize = 0.0F
            buttons[i].setOnClickListener {

                if (buttons[i].text == "cardBack" && !turnOver) {
                    buttons[i].setBackgroundResource(images[i])
                    buttons[i].setText(images[i])
                    if (clicked == 0) {
                        lastClicked = i
                    }
                    clicked++
                } else if (buttons[i].text != "cardBack") {
                    buttons[i].setBackgroundResource(cardBack)
                    buttons[i].text = "cardBack"
                    clicked--
                }

                if (clicked == 2) {
                    turnOver = true
                    if (buttons[i].text == buttons[lastClicked].text) {
                        buttons[i].isClickable = false
                        buttons[lastClicked].isClickable = false
                        turnOver = false
                        clicked = 0
                    }
                } else if (clicked == 0) {
                    turnOver = false
                }
                success()
            }

        }
    }
        private fun success() {
            if (!button1.isClickable &&
                !button2.isClickable &&
                !button3.isClickable &&
                !button4.isClickable &&
                !button5.isClickable &&
                !button6.isClickable &&
                !button7.isClickable &&
                !button8.isClickable &&
                !button9.isClickable &&
                !button10.isClickable &&
                !button11.isClickable &&
                !button12.isClickable
            ) {
                chronometer.stop()
                Toast.makeText(applicationContext, "Congratulations!", Toast.LENGTH_SHORT).show()

            }
        }






}
