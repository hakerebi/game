package com.example.game

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        auth = FirebaseAuth.getInstance()
        init()

    }

    private fun init() {
        readData()
        LogInButton.setOnClickListener {
            signIn()
            saveData()
        }
        SignUpButton.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java).apply {

            }
            startActivity(intent)
        }
    }


    private fun signIn() {
        if (LogInEdit.text.isEmpty() && PasswordEdit.text.isEmpty()) {
            Toast.makeText(applicationContext, "Please fill all fields", Toast.LENGTH_SHORT)
                .show();
        } else
            auth.signInWithEmailAndPassword(LogInEdit.text.toString(), PasswordEdit.text.toString())
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("success", "signInWithEmail:success")

                        val intent = Intent(this, MainActivity::class.java).apply {}
                        intent.putExtra("email", LogInEdit.text.toString())
                        intent.putExtra("password", PasswordEdit.text.toString())
                        startActivity(intent)

                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("fail", "signInWithEmail:failure", task.exception)
                        Toast.makeText(
                            baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }
    }
    private fun saveData(){
        val sharedPreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putString("email", LogInEdit.text.toString())
        edit.putString("password", PasswordEdit.text.toString())
        edit.apply()
    }
    private fun readData(){
        val sharedPreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE)
        LogInEdit.setText(sharedPreferences.getString("email", ""))
        PasswordEdit.setText(sharedPreferences.getString("password", ""))

    }
}

